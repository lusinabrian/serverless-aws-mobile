# Serverless with AWS-Mobile

A sample demo project that uses [AWS Mobile](https://aws.amazon.com/mobile/) to demonstrate building serverless mobile applications in [React Native](https://facebook.github.io/react-native/)

## Pre-requisites

Ensure you have [npm](https://npmjs.com) or [yarn](https://yarnpkg.com) installed on your development machine.

## Getting Started

First clone the repository:

```bash
git clone https://github.com/serverlessmobile-aws.git
```

Install dependencies

```bash
yarn install
# or
npm install
```

> Installs dependencies and sets them in the node_modules directory

You will need to configure AWS, you can check on [this video](https://www.youtube.com/watch?v=MpugaNKtw3k) for how to configure AWS IAM roles which will help in the configuration of AWS Mobile CLI from end to end (of course you will need to have an account with AWS).

If you already have AWS configured, run the following command:

```bash
yarn aws:configure
# or
npm run aws:configure
```

> This will setup the AWS Mobile CLI, which will ask you for you accessKeyId, secretAccessKey and region, if you have already set this globally, in an aws config file, these will already be pre-populated

After that has bee configured and setup, you can now run the following command:

```bash
npm run aws:init
# or
yarn aws:init
```

> This will ask you some questions and help you in creating a serverless mobile application backend. This will also install a couple of dependencies.

In addition to this, you will notice a __aws-exports.js__ file created at the root of the project as well as the __awsmobilejs__ folder at the root of the project. The file will contain credentials associated with your AWS account, ensure it is not pushed to your VCS (Github, Bitbucket, Gitlab)

At this point, you should have a configured project on AWS, you can view this project in AWS, by running the following command:

```bash
yarn aws:console
# or
npm run aws:console
```

> This will open your default browser and you will be prompted for a password to login to your AWS dashboard

### Authentication

You will need to configure and enable user sign in:

```bash
yarn aws:user-signin-enable
# or
npm run aws:user-signin-enable
```

Once user sign in is enabled, you can push the configuration

```bash
yarn aws:push
# or
npm run aws:push
```

This will enable user sign in through [Amazon Cognito](https://aws.amazon.com/cognito/).

### Storage

You can enable storage with:

```bash
npm run aws:user-files-enable
# or
yarn aws:user-files-enable
```

> This enables storage on [AWS S3](https://aws.amazon.com/s3/)

After which, you push the configuration

```bash
yarn aws:push
# or
npm run aws:push
```

You can now use storage of user files on AWS S3.

### Cloud API (Lambda Functions)

You can enable `lambda functions` using the __cloud-api enable__ on the __awsmobile cli__ tool:

```bash
yarn aws:cloud-api-enable
# or
npm run aws:cloud-api-enable
```

> awsmobile cloud-api enable will automatically create an API in API Gateway as well as a Lambda function and associate the two together.

This will create a [cloud-api](./awsmobilejs/backend/cloud-api) in the awsmobilejs folder. You can then edit the [app.js](./awsmobilejs/backend/cloud-api/sampleLambda/app.js) file to fit your needs.

After you are done, you can then push the configuration with:

```bash
yarn aws:push
# or
npm run aws:push
```

References:

1. [Blog Post](https://medium.com/react-native-training/building-serverless-mobile-applications-with-react-native-aws-740ecf719fce)