import React, { Component} from 'react';
import Amplify from "aws-amplify";
import config from "../aws-exports";
import { Text, View } from 'react-native';
import {styles} from './styles';
import { withAuthenticator } from "aws-amplify-react-native";
import { API } from "aws-amplify";

Amplify.configure(config)

class App extends Component {

    async componentDidMount() {        
        const data = await API.get("sampleCloudApi", "/items");
        console.log("data: ", data);
    }

    render() {
        return (
            <View style={styles.container}>
            <Text>Open up App.js to start working on your app!</Text>
            <Text>Changes you make will automatically reload.</Text>
            <Text>Shake your phone to open the developer menu.</Text>
            </View>
        );
    }
}

export default withAuthenticator(App);